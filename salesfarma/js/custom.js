// JavaScript Document
(function(jQ){
jQ(document).ready(function(){
	jQ(".form-group select").each(function(){
			jQ(this).wrap("<span class='select-wrapper'></span>");
			jQ(this).before("<span class='holder'></span>");
			var selectedOption = jQ(this).find(":selected").text();
			jQ(this).prev(".holder").text(selectedOption);  
			var selecteclass = jQ(this).attr('class');
			var selectestyle = jQ(this).attr('style');
			jQ(this).parent().addClass(selecteclass);
			jQ(this).parent().attr('style',selectestyle);
		jQ(this).change(function(){
			var selectedOption = jQ(this).find(":selected").text();  
			jQ(this).prev(".holder").text(selectedOption);  
			var selecteclass = jQ(this).attr('class');
			var selectestyle = jQ(this).attr('style');
			jQ(this).parent().addClass(selecteclass);  
			jQ(this).parent().attr('style',selectestyle);
		});
	});
function setTrHeight(){
	  jQ(".viewless-btn").hide();
	  jQ(".viewmore-btn").click(function(){
		  
		  var trHeight = jQ(this).parents(".viewmore-wrap").find("td.data").outerHeight();
		  var trlength = jQ(this).parents(".viewmore-wrap").find("td").length;
  		  if(jQ(window).width() < 767){
			jQ(this).parents(".viewmore-wrap").css("height",(trHeight+3)*trlength);  
			  }else{
			jQ(this).parents(".viewmore-wrap").css("height",(trHeight+1)*trlength);    
			  }
		  
		  jQ(this).hide();
		  jQ(this).parents(".viewmore-wrap").find(".viewless-btn").show();
	  });
	  
	  jQ(".viewless-btn").click(function(){
		  jQ(this).parents(".viewmore-wrap").css("height", 160);
		  jQ(this).hide();
		  jQ(this).parents(".viewmore-wrap").find(".viewmore-btn").show();
	  })
}
function setTdHeight(){	
	  var max = 0;
	  var max2 = 0;
	  jQ("td.data").each(function() {
		  var h = 0, Sh = 0;
		  //alert(h+"==="+Sh);
		  var h = jQ(this).outerHeight(); 
		  var Sh = jQ(this).find("span.title").innerHeight(); 
		  //alert(h+"==="+Sh);
		  if(h > Sh){
		  max = h	
		  }else {
		  max = Sh		
		  }
		  jQ(this).css("height", max);
		  jQ(this).find("span.title").css("height", max);
		  
		  max = 0;
	  });
}
jQ( window ).resize(function() {
	setTrHeight();
	setTdHeight();
});
jQ( window ).load(function() {
	setTrHeight();
	setTdHeight();
});
jQ('.view_btn').click(function(){
		jQ(this).prev().slideToggle( "slow", function() {
			if(jQ(this).next().find('img').attr('src')=='images/view_more1.jpg')
			{
    	jQ(this).next().find('img').attr('src','images/view_more2.jpg')
			}else{
			jQ(this).next().find('img').attr('src','images/view_more1.jpg')
			}
  });
		//jQ(this).addClass('hideex');
});

//Checkbox and Radio
jQ(function() {
    var inputOn = 'check_on',
        radioOn = 'radio_on',
        elms=jQ('input[type="checkbox"], input[type="radio"]');
        
    function setLabelClass() {
        elms.each(function(i,e) {
            jQ(e).parent('label')[e.checked?'addClass':'removeClass'](jQ(e).is(':radio')?radioOn:inputOn);
        });
    }
    elms.on('change', setLabelClass);
    setLabelClass();
});

  var getMenuName = jQ(".tabs-menu a").parents(".tabs-menu").find("li.current > a").text();
  jQ(".mob-tabmenu > span").html(getMenuName);
  jQ(window).resize(function(){
	if(jQ(window).width() < 991){
		jQ(".tabs-menu").hide();
	}else {
		jQ(".tabs-menu").show();
	}
  });
  jQ(".tabs-menu a").click(function(event) {
      event.preventDefault();
      jQ(this).parent().addClass("current");
      jQ(this).parent().siblings().removeClass("current");
      var tab = jQ(this).attr("href");
      jQ(".tab-content").not(tab).css("display", "none");
      jQ(tab).fadeIn();
		
		var getMenuName = jQ(this).parents(".tabs-menu").find("li.current > a").text();
		jQ(this).parents(".tab-nav").find(".mob-tabmenu > span").html(getMenuName);
		
		if(jQ(window).width() < 991){
			jQ(this).parents(".tabs-menu").slideUp();
		}
    });
	
   jQ(".mob-tabmenu").click( function(){
		jQ(this).next(".tabs-menu").slideToggle();
   });	
	
	
	jQ( 'a[href="#"]' ).click( function(e) {		
	   	e.preventDefault();
	});
});

})(jQuery);